//import { firstQuery } from './database';
// import fs, { browseFolder } from './first';

// //browseFolder('./src');

// firstQuery().then(data => console.log(data));

import {server} from "./server";

const port = process.env.PORT || 3000;

server.listen(port, () => {
    console.log('Serveur en écoute sur le port 3000')
})

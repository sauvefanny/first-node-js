export class Student{
    id;
    name;
    firstName;
    birthDate;

    constructor ( name, firstname, birthdate, id = null){
            this.id = id;
            this.name = name;
            this.firstName = firstname;
            this.birthDate = birthdate;
    }
}
import fs from 'fs/promises';
/**
 * Fonction qui pacours un dossier
 * @param {string} path chemin du dossier a parcourir
 */
export async function browseFolder(path) {

    let folderContent = await fs.readdir(path);

    for (let item of folderContent) {
        let itemStats = await fs.stat(item);
        console.log(itemStats.isFile());
        if (itemStats.isFile()) {
            let content = await fs.readFile(item, 'utf-8')
            console.log(content);
        }
    }
}



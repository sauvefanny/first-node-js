import { createConnection } from "mysql2/promise";
import { Student} from './class/Student';

export async function firstQuery() {
    const connection = await createConnection({
        host:'localhost',
        database: 'first_db',
        user:'simplon',
        password:'1234'

    });

const [rows] = await connection.execute('SELECT * FROM student');
const students = [];
for (const row of rows) {
    let instance = new Student( row.name, row.firstname, row.birthdate, row.id)
    students.push(instance);
}
return students;
}


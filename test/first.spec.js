import request from "supertest";
import {server} from "../src/server"

describe('First api tests', ()=> {


    it('should send success on /api/first', async () => {
        await request(server)
            .get('/api/first')
            .expect(200)
            .expect('Content-Type',/json/)

    });

    it('should return a message on url /api/first', async () => {
        let response = await request(server)
            .get('/api/first');

            
        expect(response.body).toEqual({message: 'Hello, World!'});
    });
});
